﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Perprogbeadando_01
{
    public enum UserType
    {
        easy = 1, normal = 2, hard = 3
    }

    public class User
    {
        static Random rnd = new Random();
        static int id = 0;

        public List<User> Neighbours { get; set; }
        public bool ActiveUser { get; set; }
        public bool Vizsgal { get; set; }
        public Task Task { get; set; }
        public int Name { get; set; }

        UserType type;
        int i, j;
        

        public User(bool active, UserType type)
        {
            Neighbours = new List<User>(8);
            ActiveUser = active;
            Vizsgal = true;
            Task = new Task(() => {
                while (!ActiveUser)
                {
                    AnalyzeNeighbours();
                }}, TaskCreationOptions.LongRunning);
            Name = id++;
            this.type = type;
        }
        
        public double Pay()
        {
            Family family = this.GetFamily();
            if (family.ActiveFamilyMembers() > 5)
            {
                return family.FamilyPay;
            }

            return ActiveUser ? 4.99 : 0;
        }

        public void GetNeighbours(User[,] users)
        {
            int[] index = users.IndexOf(this);
            i = index[0];
            j = index[1];
            
            List<int[]> indexes = new List<int[]> {
                new int[] { i - 1, j - 1 },
                new int[] { i - 1, j },
                new int[] { i - 1, j + 1 },
                new int[] { i, j - 1 },
                new int[] { i, j + 1 },
                new int[] { i + 1, j - 1 },
                new int[] { i + 1, j },
                new int[] { i + 1, j + 1 }
            };

            Parallel.ForEach(indexes, item => {
                    try
                    {
                        Neighbours.Add(users.ElementAt(item[0], item[1]));
                    }
                    catch (Exception)
                    {
                    }
            });
        }

        public void AnalyzeNeighbours()
        {
            bool switchToActive = false;
            int res = Neighbours.Count(x => x.ActiveUser);
            bool chance = rnd.Next(0, 101) > ((int)type * 30);

            if (Neighbours.Count == 8)
            {
                switch (type)
                {
                    case UserType.easy:
                        switchToActive = res >= 2 && chance;
                        break;
                    case UserType.normal:
                        switchToActive = res >= 3 && chance;
                        break;
                    case UserType.hard:
                        switchToActive = res >= 6 && chance;
                        break;
                }
            }

            else if (Neighbours.Count == 5)
            {
                switch (type)
                {
                    case UserType.easy:
                        switchToActive = res >= 1 && chance;
                        break;
                    case UserType.normal:
                        switchToActive = res >= 2 && chance;
                        break;
                    case UserType.hard:
                        switchToActive = res >= 3 && chance;
                        break;
                }
            }

            else if (Neighbours.Count == 3)
            {
                switchToActive = res >= 1;
            }

            ActiveUser = ActiveUser ? ActiveUser : switchToActive;
            
            ToConsole();
        }

        public void ToConsole()
        {
            lock (Console.Out)
            {
                Console.SetCursorPosition(i * 3, j);
                ConsoleColor color = ConsoleColor.White;
                if (ActiveUser)
                {
                    switch (type)
                    {
                        case UserType.easy:
                            color = ConsoleColor.Red;
                            break;
                        case UserType.normal:
                            color = ConsoleColor.Green;
                            break;
                        case UserType.hard:
                            color = ConsoleColor.Blue;
                            break;
                    }
                    
                    //color = ConsoleColor.Green;
                }
                else
                {
                    switch (type)
                    {
                        case UserType.easy:
                            color = ConsoleColor.DarkRed;
                            break;
                        case UserType.normal:
                            color = ConsoleColor.DarkGreen;
                            break;
                        case UserType.hard:
                            color = ConsoleColor.DarkBlue;
                            break;
                    }
                    //color = ConsoleColor.Red;
                }

                Console.ForegroundColor = color;
                Console.Out.WriteAsync(" x ");
            }
        }
    }
}
