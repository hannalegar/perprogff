﻿using System;
using System.Threading.Tasks;

namespace Perprogbeadando_01
{
    public static class Extensions
    {
        public static int[] IndexOf(this User[,] users, User user)
        {
            int i = 0, j = 0;
            bool ret = false;
            
            while(i < users.GetLength(0) && !ret)
            {
                j = 0;
                while(j < users.GetLength(1) && !ret)
                {
                    if (users[i, j] == user)
                    {
                        ret = true;
                    }
                    j++;
                }
                i++;
            }
            
            return new int[2] { i - 1, j - 1 };
        }

        public static User ElementAt(this User[,] users, int i, int j)
        {
            if (i > -1 && i < users.GetLength(0))
            {
                if (j > -1 && j < users.GetLength(1))
                {
                    return users[i, j];
                }
            }

            throw new ArgumentException($"There is no element at users[{i}, {j}]");
        }
        
        public static bool AnyInActive(this User[,] users)
        {
            int i = 0, j;
            bool res = false;
            while(i < users.GetLength(0) && !res)
            {
                j = 0;
                while(j < users.GetLength(1) && !res)
                {
                    if (!users[i, j].ActiveUser)
                    {
                        res = true;
                        break;
                    }
                }
            }

            return res;
        }

        public static Family GetFamily(this User user)
        {
            Family family = null;
            Parallel.ForEach(Spotify.families, x =>
            {
                if (x.familyMembers.Contains(user))
                {
                    family = x;
                }
            });

            return family;
        } 
    }
}
