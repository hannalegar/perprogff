﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Perprogbeadando_01
{
    public class Family
    {
        static int id = 0;

        public List<User> familyMembers { get; set; }

        public double FamilyPay
        {
            get { return 7.99; }
        }

        public int ID { get; set; }

        public Family()
        {
            ID = id++;
        }

        public void AddFamilyMember(User user)
        {
            if (familyMembers == null)
            {
                familyMembers = new List<User>();
            }

            familyMembers.Add(user);
        }

        public int ActiveFamilyMembers()
        {
            return familyMembers.Count(x => x.ActiveUser);
        }
    }
}
