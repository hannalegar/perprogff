﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Perprogbeadando_01
{
    public class Spotify
    {
        static Random rnd = new Random();
        static int width = 50, height = 50;

        public static List<Family> families;

        public User[,] UserArray => userArray;
        public Task payments;
        
        User[,] userArray;
        int familyNumber = rnd.Next(5, (width * height) / 5);
        double sum = 0;

        public Spotify()
        {
            families = new List<Family>(familyNumber);
            CreateFamilies();

            userArray = new User[width, height];
            CreateUsers();

            payments = new Task(() =>
            {
                while (userArray.AnyInActive())
                {
                    Parallel.For(0, userArray.GetLength(0), i => {
                        Parallel.For(0, userArray.GetLength(1), j =>
                        {
                             sum += userArray[i, j].Pay();
                        });
                    });
                }
            }, TaskCreationOptions.LongRunning);
        }

        void CreateFamilies()
        {
            Parallel.For(0, familyNumber, i =>
            {
                families.Add(new Family());
            });
        }

        void CreateUsers()
        {
            bool active;
            UserType userType;
            int whichFamily;

            for (int i = 0; i < userArray.GetLength(0); i++)
            {
                for (int j = 0; j < userArray.GetLength(1); j++)
                {
                    active = rnd.Next(0, 101) > 87;
                    whichFamily = rnd.Next(0, familyNumber);
                    userType = GetUserType();
                    userArray[i, j] = new User(active, userType);
                    families[whichFamily].AddFamilyMember(userArray[i, j]);
                }
            }
        }

        private UserType GetUserType()
        {
            int num = rnd.Next(0, 101);

            if (num <= 20)
            {
                return UserType.easy;
            }

            else if (num <= 95)
            {
                return UserType.normal;
            }

            else
            {
                return UserType.hard;
            }
        }

        public void AddNeighboursToUser()
        {

            payments.Start();
            Parallel.For(0, userArray.GetLength(0), i => {
                Parallel.For(0, userArray.GetLength(1), j =>
                {
                    userArray[i, j].GetNeighbours(userArray);
                });
            });
        }

        public void AnalyzeUsers()
        {
            Parallel.For(0, userArray.GetLength(0), i =>
            {
                Parallel.For(0, userArray.GetLength(1), j =>
                {
                    userArray[i, j].Task.Start();
                });
            });
        }

        public void ToConsole()
        {
            Parallel.For(0, userArray.GetLength(0), i => {
                Parallel.For(0, userArray.GetLength(1), j =>
                {
                    userArray[i, j].ToConsole();
                });
            });
        }
    }
}
